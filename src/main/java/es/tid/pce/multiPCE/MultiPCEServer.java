package es.tid.pce.multiPCE;

public class MultiPCEServer {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        MultiDomainPCEServer pceserver = new MultiDomainPCEServer();
        if (args.length > 0)
            pceserver.configure(args[0]);
        else
            pceserver.configure(null);

        pceserver.run();
    }

}
